class MainController < ApplicationController
  def index
  end

  def new
    @twobytwo = Calculator.send(:twobytwo, *[params[:twobytwomsg], params[:operation], params[:a1], params[:b1], params[:c1], params[:d1]])
   
    @brute = Calculator.send(:brute, *[params[:text]])

    @hardmode = Calculator.send(:hardmode, *[params[:hmmsg], params[:operation]])
    
    @threebythree = Calculator.send(:threebythree, *[params[:threemsg], params[:operation], params[:x3a1], params[:x3b1], params[:x3c1], params[:x3a2], params[:x3b2], params[:x3c2], params[:x3a3], params[:x3b3], params[:x3c3]])
    render :index
  end

end
