require 'Hunspell'
require 'matrix'

class Calculator < ApplicationRecord
  $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

  $hm_letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '?', '!']

  # Initialize values for 2x2 matrix hill decryption and encryption
  def self.twobytwo(str, operation, a1, b1, c1, d1)
    return nil if str == nil
    
    # Strip white space for encryption algorithm, 
    # but preserve white space in variable "given" 
    # so that it can be added back into the result
    characters = str.gsub(/\s+/, "").chomp.upcase.chars
    @given = str.chomp.upcase.chars

    # Pad the given string with a Z if there aren't an even number of characters
    if characters.count % 2 != 0
      characters << 'Z'
    end

    # Convert our phrase into numbers
    @nums = Array.new
    for i in characters
      @nums.push(letter_convert(i))
    end

    # Initialize the encryption/decryption matrix
    @matrix1 = Matrix[
      [a1.to_i, b1.to_i],
      [c1.to_i, d1.to_i]
    ] 

    # Route to the correct operation
    if operation == "encrypt"
      solution = encrypt
    else
      solution = decrypt(2)
    end
    
    # If there was whitespace in the original string, insert it here
    for i in 0..@given.length
      if @given[i] == ' '
        solution.insert(i, ' ')
      end
    end

    solution.join
  end

  # Initialize values for hard mode matrix hill decryption and encryption
  def self.hardmode(str, operation)
    return nil if str == nil

    characters = str.gsub(/\s+/, "").chomp.upcase.chars

    # Pad with Z if character number isn't even
    if characters.count % 2 != 0
      characters << 'Z'
    end

    # Initialize our two matrices to encrypt/decrypt in succession
    @matrix1 = Matrix[
      [3, 11],
      [4, 15]
    ]

    @matrix2 = Matrix[
      [10, 15],
      [5, 9]
    ]

    @nums = Array.new
    
    # Route to the correct operation
    if operation == "encrypt"
      # Convert our phrase into numbers
      for i in characters
        @nums.push(letter_convert(i))
      end

      hm_encrypt
    else
      for i in characters
        @nums.push(hm_letter_convert(i))
      end

      hm_decrypt
    end
  end

  # Initialize values for 3x3 matrix hill decryption and encryption
  def self.threebythree(str, operation, a1, b1, c1, a2, b2, c2, a3, b3, c3)
    return nil if str == nil
    
    # Strip white space for encryption algorithm, 
    # but preserve white space in variable "given" 
    # so that it can be added back into the result
    characters = str.gsub(/\s+/, "").chomp.upcase.chars
    @given = str.chomp.upcase.chars

    # Pad the given string with a Z if the numbers of characters is not divisible by 3
    mod = characters.count % 3
    if mod != 0
      until mod == -1 do
        characters << 'Z'
        mod -= 1
      end
    end

    # Convert our phrase into numbers
    @nums = Array.new
    for i in characters
      @nums.push(letter_convert(i))
    end

    # Initialize the encryption/decryption matrix
    @matrix1 = Matrix[
      [a1.to_i, b1.to_i, c1.to_i],
      [a2.to_i, b2.to_i, c2.to_i],
      [a3.to_i, b3.to_i, c3.to_i]
    ] 

    # Route to the correct operation
    if operation == "encrypt"
      solution = encrypt_three
    else
      solution = decrypt(3)
    end
    
    # If there was white space in the original string, insert it here
    for i in 0..@given.length
      if @given[i] == ' '
        solution.insert(i, ' ')
      end
    end

    solution.join
  end

  # Fill an array with 1000 brute force decryption attempts to print
  def self.brute(str)
    return nil if str == nil
    max = 90000
    attempts = Array.new

    # Sort attempts by whether or not they spell an English word
    sp = Hunspell.new(File.join(File.dirname(__FILE__), 'en_US.aff'), File.join(File.dirname(__FILE__), 'en_US.dic'))

    # Initialize random numbers from 1 to 10 for each element of matrix
    a1 = (0..10).to_a.shuffle
    b1 = (0..10).to_a.shuffle
    a2 = (0..10).to_a.shuffle
    b2 = (0..10).to_a.shuffle

    # N^4 loop - calculation may be slow, thus capped at the max
    catch :abort do
      for i in a1
        for j in b2
          for k in a2
            for l in b2
              begin
                attempts.push(twobytwo(str, "decrypt", l, k, j, i) + " with " + Matrix[
                  [l, k],
                  [j, i]
                ].to_s)
              rescue
                next
              end
              throw :abort unless max > 0
              max -= 1
            end
          end
        end
      end
    end

    attempts.sort_by{ |item| sp.spellcheck(item.partition(" ").first) ? 0 : 1 }.take(500)
  end

  # Convert characters to their respective numbers
  def self.letter_convert(char)
    for i in 0..$letters.length
      return i if $letters[i] == char
    end
  end

  # Convert numbers to characters
  def self.int_convert(num)
    $letters[num]
  end
  
  # Convert characters to their respective numbers in hard mode
  def self.hm_letter_convert(char)
    for i in 0..$hm_letters.length
      return i if $hm_letters[i] == char
    end
  end

  # Convert numbers to characters in hard mode
  def self.hm_int_convert(num)
    $hm_letters[num]
  end

  # Encrypt a string by standard working modulo 26
  def self.encrypt
    # Hold the encrypted string in an array
    solution = Array.new

    # Split every two numbers, put them in a matrix,
    # then perform matrix multiplication. Convert back to characters by mod 26.
    i = 0
    while i < @nums.length
      char_mat = Matrix[
        [@nums[i]],
        [@nums[i+1]]
      ] 

      sol = @matrix1 * char_mat
      sol.each { |item| solution.push(int_convert(item % 26)) }

      i += 2
    end

    solution
  end

  # Encrypt a string through the two specified matrices
  def self.hm_encrypt
    # Hold the encrypted string in an array, first encrypt in standard way
    solution = Array.new
    @nums = encrypt.map { |a| letter_convert(a) }

    # Matrix multiplication, convert back to characters mod 29
    i = 0
    while i < @nums.length
      char_mat = Matrix[
        [@nums[i]],
        [@nums[i+1]]
      ]

      sol = @matrix2 * char_mat
      sol.each { |item| solution.push(hm_int_convert(item % 29)) }

      i+=2
    end

    "Result " + solution.join + " from intial encryption of " + @nums.map{|a| int_convert(a)}.join
  end
  
  def self.encrypt_three
    # Hold the encrypted string in an array
    solution = Array.new

    # Split every thre numbers, put them in a matrix,
    # then perform matrix multiplication. Convert back to characters by mod 26.
    i = 0
    while i < @nums.length
      char_mat = Matrix[
        [@nums[i]],
        [@nums[i+1]],
        [@nums[i+2]]
      ] 

      sol = @matrix1 * char_mat
      sol.each { |item| solution.push(int_convert(item % 26)) }

      i += 3
    end

    solution
  end

  # Decryption is the same process as encryption, but with the inverse of the encryption matrix (det mod 26 incorporated)
  def self.decrypt(size)
    @matrix1 = inverse(@matrix1, 26)
    return encrypt if size == 2
    encrypt_three
  end

  # Decrypt through mod 29 then standard mod 26 decryption
  def self.hm_decrypt
    @matrix2 = inverse(@matrix2, 29)
    @matrix1 = inverse(@matrix1, 26)
    solution = Array.new

    i = 0
    while i < @nums.length
      char_mat = Matrix[
        [@nums[i]],
        [@nums[i+1]]
      ]

      sol = @matrix2 * char_mat
      sol.each { |item| solution.push(item % 29) }
      i += 2
    end

    @nums = solution
    encrypt.join
  end

    # The matrix class has a built in inverse method, but we want to incorporate the modulo in inverse calculations
    def self.inverse(matrix, mod)
      detinv = modinv(matrix.det % mod, mod)

      # Get the adjugate of the encryption matrix
      adj = matrix.adjugate.map do |element|
        if element < 0
          element + mod
        else
          element
        end
      end

      matrix = adj.map{ |e| (e * detinv) % mod }
    end

    # Method to find modular inverses, given from rosetta code
    def self.modinv(a, m) # compute a^-1 mod m if possible
      raise "NO INVERSE - #{a} and #{m} not coprime" unless a.gcd(m) == 1
      return m if m == 1
      m0, inv, x0 = m, 1, 0
      while a > 1
        inv -= (a / m) * x0
        a, m = m, a % m
        inv, x0 = x0, inv
      end
      inv += m0 if inv < 0
      inv
    end

  end
