Rails.application.routes.draw do
  get 'main/index'
  post 'main/index', action: :new, controller: 'main'

  root 'main#index'
end
