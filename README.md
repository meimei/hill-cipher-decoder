# Hill Cipher Decoder and Encoder

This is a simple hill-cipher encoder and decoder built with Ruby on Rails (ruby 2.7.1p83 [x86_64-linux]). 
You will need hunspell, bundler, and ruby installed on your system prior to building.

To build, enter the directory, fetch dependencies with 
```bundle install```, and start up with ```rails server```. 

This hill-cipher tool will encrypt and decrypt with 2x2 matrices, 3x3 matrices, and hard mode double matrix encryption: first by 

```math
\begin{bmatrix}
    3 & 11 \\
    4 & 15 \\
\end{bmatrix}
``` 

working modulo 26, then by

```math
\begin{bmatrix}
10 & 15 \\
5 & 9
\end{bmatrix}
``` 

working modulo 29.

The interface will also attempt to brute-force decrypt with a 2x2 matrix by fuzzing random matrix values, then matching entries that are in an English dictionary.
